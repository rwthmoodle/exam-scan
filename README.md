<code> <b>This repository has been updated on 08.05.2023 to support RWTHmoodle 4.x versions after bugfix MDL-76309 has been deployed.</b> </code>

# exam-scan

Exam-Scan is a command-line tool that helps to watermark and/or encrypt feedback files/scanned exams/additional exam materials and prepare them in Moodle uploadable format.
The tool is designed to handle zipped submissions downloadable from Moodle as well as scanned PDFs from your local scanner.

## Contents

* <details><summary><mark>importsubmissions.py<i>(*for downloadable PDFs*)</i></mark></summary> unzips files from submission zip file downloadable from Moodle(in case there are no scans) and renames it accordingly in the format <i> {Matriculation number}_{first letter of Lastname}</i></details>

* <details><summary><mark>renamescans.py<i>(*for scanned PDFs*)</i></mark></summary> Rename scanned PDFs, assuming scan order equal to alphabetical order of students in Moodle grading sheet, such that the file name is in the format in the format <i> {Matriculation number}_{first letter of Lastname}</i>. This only works if exams were scanned in alphabetical order. Optionally, each scanned PDF is searched for barcodes/QRs containing the matriculation number to double check. </details>
* <details><summary><mark>supplements.py </mark></summary>renames and create copies of sample solutions(if any)/additional exam materials for every student. </details>
* <details><summary><mark>watermark.py  </mark></summary>watermarks each page of PDFs containing exam scans with matriculation number of the respective student </details>
* <details><summary><mark>encrypt.py </mark></summary> encrypts PDFs either with a common password(passed as an argument) or a randomly generated password(when there is no argument)</details>
* <details><summary><mark>preparemoodleupload.py </mark></summary> zips PDFs in the format acceptable for moodle upload via assign module as feedback file for each student</details>
* <details><summary><mark>batch.py </mark></summary> executes all three programs as a singular batch job</details>

Please note that `importsubmissions.py`, `supplements.py`, `watermark.py`, `encrypt.py`, `preparemoodleupload.py` do not depend on each other.
If you want to use only a subset (or one) of the scripts, you can do so after installing the corresponding script's [dependancies](Dependancies.md).

Exemplary outputs can be downloaded:

* [moodle_feedbacks.zip](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/jobs/artifacts/master/raw/out/moodle_feedbacks.zip?job=test): The zip-Archive to be uploaded to Moodle containing the watermarked and encrypted PDFs for each student.
* [passwords.csv](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/jobs/artifacts/master/raw/out/passwords.csv?job=test): CSV file containing passwords for each PDF.

For more info please refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/)
</details>

## Instructions

### Prerequisites

1. <details><summary><b>Create and setup Moodle</b></summary><ul><li> In your Moodle course room, create an `assign` module following this [guideline](https://help.itc.rwth-aachen.de/service/8d9eb2f36eea4fcaa9abd0e1ca008b22/article/0cfca4212fef4712ad2d432ac83eaf3e)</li><li> Download the grading table `Grades.csv` from Moodle via: `View all submissions` &#8594; `Grading action` &#8594; `Download grading worksheet`</li></ul></details>

2. <details><summary><b>Create PDFs corresponding to each exam</b></summary><b><i>Scanned Exams</i></b><ol><li>Scan the exams and save the scans as PDFs (each page should be A4). For most copy machines, you can save an A3 scan (double page of an exam) as two A4 pages.</li><li>You can either<ol><li>Order the scans following the same order as the grading worksheet</li><li>Or rename the scans in the format <i>{Matriculation number}_{first letter of Lastname}</i> (e.g. <mark>123456_Nachname.pdf</mark>/<mark>123456_L.pdf</mark>).</li></ol></ol><b><i>OR: Download submission zip file</i></b>  Download the submission zip file from Moodle via:<ol><li>Assignment Main Page->View all submissions</li><li>Mark 'Download submissions in folders'</li><li>Choose 'Dowload all submissions' from Grading action dropdown</li></ol></details>

3. <details><summary><b>OPTIONAL: Create sample solutions/additional exam materials (Refer [here](https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/issues/3))</b></summary><ol><li>Scan the sample solutions/additional exam materials and save the scans as PDFs (each page should be A4). For most copy machines, you can save an A3 scan (double page of an exam) as two A4 pages.</li><li>Place all PDFs in a folder, e.g. <mark>supplements</mark>.</li></ol></details>

4. <details><summary><b>Install the software dependancies</b></summary>The current version of code was tested on Windows10, Ubuntu 20.04.1 LTS and macOS 10.14 Mojave to ensure platform independence.The code has the following software dependencies which needs to installed before the programs can be run successfully.<ul><li>Imagemagick (version: 7.0.10-33)</li><li>Ghostscript (version: 9.53.3)</li><li>Python (version: 3.8/3.9)</li><li>PIP (version: 21.0.1)</li><li>Additional Python modules:<ul><li>wand (version 0.6.5)</li><li>pillow (version: 8.1.0)</li><li>pwgen (version: 0.8.2.post0)</li><li>pikepdf (version 2.5.0)</li><li>zip (version 0.02)</li></ul></li></ul></details>

  >Instructions to install software dependencies based on your operating system: <ul><li> Windows 10 : [Installation of Software Dependencies](docs/swdependencies_win.md)</li><li> MacOS : [Installation of Software Dependencies](docs/swdependencies_mac.md)</li><li>Linux : [Installation of Software Dependencies](docs/swdependencies_linux.md)</li></ul>

### Experienced User

#### Virtual Environment for experienced users 
If you are an experienced user and familiar with the python `venv` (virtual environments) module and after having installed both ImageMagick (beware the `Policy Error` fix in [FAQs])and Ghostscript you can install the python dependencies in the virtual environment via pip with

  ```bash
  python -m venv venv
  source venv/bin/activate
  pip install -r requirements.txt 
  ```


#### Docker

If you are an experienced user familiar with Docker, you can use the provided `Dockerfile` to easily run the scripts.
Either use the already built image

```bash
docker run --name='examscan' --rm -v $(pwd):$(pwd) -w $(pwd) registry.git.rwth-aachen.de/rwthmoodle/exam-scan:master batch.py --help
```

or build the Dockerfile yourself locally

```bash
docker build -t examscan:latest .
docker run --name examscan --rm -v $(pwd):$(pwd) -w $(pwd) examscan:latest batch.py --help
```

## Commands

#### Import submission from moodle zip file `Moodle4.x updated`

Unzip submission files from assign module and rename them : Assuming that `./tests/assets/newmoodle/submissions-moodle.zip` is the zip file containing all submissions and `./tests/assets/newmoodle/grades.csv` is the grading worksheet and `./tests/assets/newmoodle/pdfs` is the output folder where the submissions are extracted to

**For Moodle 4.x**

```bash
python importsubmissions.py ./tests/assets/newmoodle/submissions-moodle.zip  ./tests/assets/newmoodle/grades.csv ./tests/assets/newmoodle/pdfs 
```

**For older(less the 4) Moodle Versions**

Assuming that `./tests/assets/submissions-moodle.zip` is the zip file containing all submissions and `./tests/assets/Grades.csv` is the grading worksheet

```bash
python importsubmissions.py ./tests/assets/submissions-moodle.zip  ./tests/assets/Grades.csv ./tests/assets/pdfs --moodle 0
```

**For Dynexite submissions:**

```bash
python importsubmissions.py ./tests/assets/submissions_dynexite.zip ./tests/assets/RWTHonline.csv ./tests/assets/pdfs -x --csvdelim ; 
```

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/importsubmissions.html)

#### (Scanned Exams) Rename scanned PDFs

Assuming that `./tests/assets/pdfs_scan` is the folder containing all scans, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/pdfs` is the output folder where all the renamed scans will be placed

```bash
python renamescans.py  ./tests/assets/pdfs_scan ./tests/assets/Grades.csv ./tests/assets/pdfs
```
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/renamescans.html)

#### Prepare copies of Sample Solutions for each student (Optional)

Assuming that the folder `./tests/assets/supplements` holds the scans of the sample solution/additional materials, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/pdfs` is the output folder where all the renamed materials will be placed

```bash
python supplements.py ./tests/assets/supplements ./tests/assets/Grades.csv ./tests/assets/pdfs
```

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/supplements.html)

#### Watermark the submissions

Assuming that the folder `./tests/assets/pdfs` holds the scans of the exams and filenames follow the format `<Matriculation number>_<Lastname/first letter of Lastname>`, `./tests/assets/pdfs_watermarked` is the output folder where all the watermarked PDFs will be placed and cores=2 indicate the number of cores for parallel processing.

```bash
python watermark.py ./tests/assets/pdfs ./tests/assets/pdfs_watermarked --cores 2 --dpi 150 --quality 75
```

**TIP:** Play around with `dpi` and `quality` parameters according to your requirements. Higher values for these two will result in high resolution PDFs of bigger size (ideal for when the number of files is low). Lower values will result in PDFs having lower file size and low resolution (ideal when the number of files is high)

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/watermark.html)

#### Encrypt the files

Assuming that the folder `./tests/assets/pdfs_watermarked` holds the files to encrypt, `./tests/assets/pdfs_encrypted` is the output folder where all the encrypted PDFs will be placed and providing a common password for all encrypted PDFs with the `--password` option

```bash
python  encrypt.py ./tests/assets/pdfs_watermarked ./tests/assets/pdfs_encrypted --password ganzgeheim
```
**TIP:** Omitting the `password` option, you can set randomly generated passwords for each PDF

For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/encrypt.html)

#### Prepare for Moodle batch upload

**For Moodle 4.x**

Assuming that the folder `./tests/assets/pdfs_encrypted` holds the scans of be uploaded to Moodle, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/out` is the output folder where zip archive will be palced

```bash
python preparemoodleupload.py ./tests/assets/pdfs_encrypted  ./tests/assets/Grades.csv  ./tests/assets/out/feedback.zip
```

**For older(less the 4) Moodle Versions**

Assuming that the folder `./tests/assets/pdfs_encrypted` holds the scans of be uploaded to Moodle, `./tests/assets/Grades.csv` is the grading worksheet and `./tests/assets/out` is the output folder where zip archive will be palced

```bash
python preparemoodleupload.py ./tests/assets/pdfs_encrypted  ./tests/assets/Grades.csv  ./tests/assets/out/feedback.zip --moodle 0
```

Then, you can upload `feedback.zip` in Moodle:
`Alle Angaben anzeigen` &#8594; `Bewertungsvorgang` &#8594; `Mehrere Feedbackdateien in einer Zip-Datei hochladen`

**NOTE:** There is a upload filesize limit in Moodle. For RWTHMoodle (on the day this README is updated), this value is 250MB. If the folder contaning the PDFs is greater than upload filesize limit, provide the limit as a parameter like `--moodleuploadlimit 250`
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/preparemoodleupload.html)


#### Batch job  `Moodle4.x updated`

You can do all the above processes with one single scripts as follows:

##### Online Submissions in Zip file #####

**For Moodle 4.x**

```bash
python batch.py ./tests/assets/newmoodle/pdfs ./tests/assets/newmoodle/grades.csv ./tests/assets/newmoodle/out --cores 2 --password ganzgeheim --zip ./tests/assets/newmoodle/submissions-moodle.zip
```

**For older(less the 4) Moodle Versions**


```bash
python batch.py ./tests/assets/pdfs ./tests/assets/Grades.csv ./tests/assets/out --cores 2 --password ganzgeheim --suppinfolder ./tests/assets/supplements --supp --zip ./tests/assets/submissions.zip --moodle 0
```

##### Scanned Exams #####

**For Moodle 4.x**


```bash
python batch.py ./tests/assets/pdfs ./tests/assets/newmoodle/grades.csv ./tests/assets/out --cores 2 --password ganzgeheim --suppinfolder ./tests/assets/supplements --supp
```

**For older(less the 4) Moodle Versions**


```bash
python batch.py ./tests/assets/pdfs ./tests/assets/Grades.csv ./tests/assets/out --cores 2 --password ganzgeheim --moodle 0 --suppinfolder ./tests/assets/supplements --supp
```

In this case the `./tests/assets/out` contains both passwords.csv and zip archive
For more info on the scripts and additional arguments refer to the [Documentation](https://rwthmoodle.pages.rwth-aachen.de/exam-scan/batch.html)

## Original Authors

Helmut Flasche, Jens Schneider, Christian Rohlfing, IENT RWTH Aachen\
Dietmar Kahlen, ITHE RWTH Aachen\
Amrita Deb, IT Center, RWTH Aachen University

## Who do I talk to?

Servicedesk IT-Center RWTH Aachen <[servicedesk@itc.rwth-aachen.de](mailto:servicedesk@itc.rwth-aachen.de)>

If you have any errors or problems while running the programs, make sure to first check if your error is listed in [FAQs]

[FAQs]: https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/blob/master/FAQs.md
