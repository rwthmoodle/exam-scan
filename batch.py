#!/usr/bin/env python

"""watermarks, encrypts and prepares PDF files for upload to Moodle assignment

Attention: Contents in output folder will be overwritten in the following!
"""

__author__ = "Amrita Deb Dutta(deb.dutta@itc.rwth-aachen.de), " +\
    "Christian Rohlfing (rohlfing@ient.rwth-aachen.de)"


import os
import sys
import time
import argparse

import utils.moodle as moodle
import shutil
import importsubmissions
import supplements
import watermark
import encrypt
import preparemoodleupload


def _make_parser():
    csv_parser = moodle.get_moodle_csv_parser()

    parser = argparse.ArgumentParser(
        parents=[csv_parser],
        description=__doc__, prog='batch.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "infolder", help="Input folder with PDFs")
    parser.add_argument(
        "csv", help="Moodle grading CSV sheet")
    parser.add_argument(
        "outfolder",
        help="output folder with passwords.csv and Moodle feedbacks ZIP")

    parser.add_argument(
        "--moodle", default="1",
        help="Is the script being run with Moodle 4 data or earlier. 1 means Moodle 4+ 0 means 3.x")
    parser.add_argument(
        "--supp", action='store_true',
        help="Flag for watermarking supplements")
    parser.add_argument(
        "--suppinfolder", default="./supplements",
        help="Input folder with supplements such as sample solutions")
    parser.add_argument(
        "--zip", default="0", help="ZIP file with submissions to be " +
        "extracted to input folder")
    parser.add_argument(
        "-e", "--cores", default="1",
        help="Number of cores for parallel processing")
    parser.add_argument(
        "-p", "--password", default="",
        help="sets global password. Default='' such that each PDF gets " +
             "custom password")
    parser.add_argument(
        "-d", "--dpi", default="150",
        help="Dots-per-inch parameter for PDF to image conversion")
    parser.add_argument(
        "-t", "--tmp", default="./tmp", help="Temporary folder")
    parser.add_argument(
        "-z", "--imgmgc", default=None,
        help="Set temporary directory for Imagemagick")

    return parser


# Create argument parser with default values
_parser = _make_parser()


def main(args):
    # Argument handling
    args = _parser.parse_args(args)
    infolder = args.infolder
    csv = args.csv
    outfolder = args.outfolder
    cores = args.cores
    dpi = args.dpi
    tmp = args.tmp
    password = args.password
    watermark_supp = args.supp
    supinfolder = args.suppinfolder
    inzip = args.zip
    csv_enc = args.csvenc
    csv_delim = args.csvdelim
    csv_quote = args.csvquote
    moodle4 = args.moodle
    imgmgckdir = args.imgmgc

    starttime = time.time()

    # Check folders
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)

    # Unzip submissions if provided zip archive
    if inzip != "0":
        if not os.path.exists(infolder):
            os.makedirs(infolder)
        importsubmissions.main([
            inzip, csv, infolder,
            '--csvenc', csv_enc, '--csvdelim', csv_delim,
            '--csvquote', csv_quote, '--moodle', moodle4])

    # Watermarking
    watermark_outfolder = os.path.join(tmp, 'pdfs_watermarked')
    if not os.path.exists(watermark_outfolder):
        os.makedirs(watermark_outfolder)
    
    if imgmgckdir is not None:
        if not os.path.exists(imgmgckdir):
            os.makedirs(imgmgckdir)

    watermark.main([
        infolder, watermark_outfolder,
        '--cores', cores, '--dpi', dpi, '--imgmgc', imgmgckdir])

    if watermark_supp:
        supoutfolder = os.path.join(tmp, 'supplements_out')
        if not os.path.exists(supoutfolder):
            os.makedirs(supoutfolder)

        supplements.main([
            supinfolder, csv, supoutfolder,
            '--csvenc', csv_enc, '--csvdelim', csv_delim,
            '--csvquote', csv_quote
            ])

        watermark.main([supoutfolder, watermark_outfolder,
                        '--cores', cores, '--dpi', dpi])

    # Encryption
    enc_out = os.path.join(tmp, 'pdfs_encrypted')
    if not os.path.exists(enc_out):
        os.makedirs(enc_out)

    passwordcsv = os.path.join(outfolder, 'passwords.csv')
    encrypt.main([watermark_outfolder, enc_out,
                  '--passwordout', passwordcsv, '--password', password])

    # ZIP Archive preparation process
    moodle_out = os.path.join(outfolder, 'moodle_feedbacks.zip')
    moodle_tmp = os.path.join(tmp, 'tmp')
    if not os.path.exists(moodle_tmp):
        os.makedirs(moodle_tmp)

    preparemoodleupload.main([
        enc_out, csv, moodle_out,
        '--tmp', moodle_tmp, '--csvenc', csv_enc, '--csvdelim', csv_delim,
        '--csvquote', csv_quote, '--moodle', moodle4])

    print(f'\nCleaning all temporary files')
    shutil.rmtree(tmp)
    endtime = time.time()
    print(f'\nTotal time taken: {endtime-starttime:.2f}s\n')


if __name__ == '__main__':
    main(sys.argv[1:])
