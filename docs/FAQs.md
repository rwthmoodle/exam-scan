# FAQs

Here we discuss  the common problems and their corresponding solutions:

## Policy Error: attempt to perform an operation not allowed by the security policy 'PDF'

<img src="/images/policyissue.png" height="100" width ="600"/>

Solution:

This occurs in Linux. This occurs if the policy.xml correction referred in [Step6] of the installation guide is not done correctly.

* Navigate to /etc/ImageMagick-6/policy.xml
* Find `<policy domain="coder" rights="none" pattern="PDF" />` and replace `"none"` by `"read|write"`

[Step6]: https://git.rwth-aachen.de/rwthmoodle/exam-scan/-/blob/platform-independant/swdependencies_linux.md

## PermissionError: [WinError 32] The process cannot access the file because it is being used by another process

This occurs in Windows. The reason is the program is trying to work with a file that is open. The error is generally followed by a filename which is open and therefore the error occurs. User must close that file

## Not all files are watermarked/encrypted or not all files are included in the final ZIP archive

This happens if the filename is not given according to the naming scheme i.e `<matriculation number>_<Nachname>`. The program will not pickup a file whose name doesn't follow this scheme.

## "Command 'python'/'python3' not found" or "'python'/'python3' is not recognized as an internal or external command, operable program or batch file."

This can happen because:

* Python is not installed
* Environmental variables for Python is not set. This issue mostly happens in Windows if one forgets to check 'Add Python to Path' [checkbox](./images/pythoninstallation.png) during installation
* You are usaing the wrong command variation. Try running the program with the other command variation.

## "Invalid Syntax"

This can be because you are running a Python version that is incompatible to the version of Python this was developed in.Check if the python version is higher than 3.0 preferrably 3.8 or 3.9.

## python: can't open file `<current directory>/watermark.py`: [Errno 2] No such file or directory

You are not in the correct folder to run the scripts. You must run the program from inside the extracted folder of the Git repository

## The ZIP archive has locked PDFs. Where can I find passwords?

If you are running individual scripts, the passwords are saved in passwords.csv under pdfs_encrypted folder.

if you are running the batch job script, the passwords are saved in `out/password.csv`.
