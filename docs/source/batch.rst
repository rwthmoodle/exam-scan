Batch
*****

.. toctree::
  :maxdepth: 3

.. autoprogram:: batch:_parser
  :prog: batch.py

API
===

.. automodule:: batch
  :members:
