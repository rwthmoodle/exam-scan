Encrypt
*******

.. toctree::
  :maxdepth: 3

.. autoprogram:: encrypt:_parser
  :prog: encrypt.py

API
===

.. automodule:: encrypt
  :members:
