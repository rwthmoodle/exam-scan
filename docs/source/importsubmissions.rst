Handle Moodle/Dynexite Submissions
************************

.. toctree::
  :maxdepth: 3

.. autoprogram:: importsubmissions:_parser
  :prog: importsubmissions.py

API
===

.. automodule:: importsubmissions
  :members:
