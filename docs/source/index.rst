Welcome to Exam Scan's documentation!
=====================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   
   encrypt
   preparemoodleupload
   importsubmissions
   renamescans
   supplements
   watermark
   batch


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
