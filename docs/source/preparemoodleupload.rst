Prepare Moodle Upload
*********************

.. toctree::
  :maxdepth: 3

.. autoprogram:: preparemoodleupload:_parser
  :prog: preparemoodleupload.py

API
===

.. automodule:: preparemoodleupload
  :members:
