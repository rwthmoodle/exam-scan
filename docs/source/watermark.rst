Watermark
*********

.. toctree::
  :maxdepth: 3

.. autoprogram:: watermark:_parser
  :prog: watermark.py

API
===

.. automodule:: watermark
  :members:
