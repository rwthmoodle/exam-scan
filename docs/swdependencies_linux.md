# Software Dependencies Installation Linux

This is a step-by-step guide to download and install the various software dependencies of the exam scan program for Linux. This version of code was tested on Ubuntu 20.04.1 LTS.

1. Check Python

   Python 3 comes already installed since Ubuntu 18.04. You can check if your system is equipped with python by opening the terminal and typing `python3 --version` to check the python version

   In case Python is not installed please install it following this documentation: [https://docs.python-guide.org/starting/install3/linux/]

2. Install Imagemagick

   Open the terminal and type the below command to install Imagemagick:

   `sudo apt-get install libmagickwand-dev`

   *Hint:* You may have to give your system password to initiate the installation and press `Y` whenever asked `Do you want to continue? [Y/n]` in the installation

   <img src="/assets/images/linux1.png" height="500" width ="500"/>

   For more details refer to this documentation: [https://docs.wand-py.org/en/latest/guide/install.html]

3. Install Ghostscript

   Open the terminal and type the below command to install Ghostscript:

   `sudo apt-get install ghostscript`

   *Hint:* Press `Y` whenever asked `Do you want to continue? [Y/n]` in the installation

4. Check or install PIP

   Type `pip3 -V` in terminal to check if PIP is installed in your system. In case it is not install it with the below command:

   `sudo apt install python3-pip`

   *Hint:* Press `Y` whenever asked `Do you want to continue? [Y/n]` in the installation

5. Install additional Python modules with pip

   Type the below command to install additional Python modules with pip:

   `pip install -r requirements.txt`

   The above command works similarly if you substituted `pip` with `pip3`.

6. Fix Imagemagick Security bug

   * Navigate to the policy.xml file of ImageMagick at /etc/ImageMagick-6/policy.xml (`cd /etc/ImageMagick-6/`)
   * Open the file and find `<policy domain="coder" rights="none" pattern="PDF" />`
      * `sudo vi policy.xml` (*Hint:* You may have to give your system password )
      * Type `/` followed by `<policy domain="coder" rights="none" pattern="PDF" />`
   * Replace it with `<policy domain="coder" rights="read|write" pattern="PDF" />` (Press `i` to go to insert mode and then do the required editing)
   * Save the file
      * To save: Press Esc followed by `:wq`.
      * To exit without saving: Incase you are not happy with the edit press Esc followed by `:q!`

Now everything should be set up.

## Note

To run the programs of exam scan use the `python3` command variations.
