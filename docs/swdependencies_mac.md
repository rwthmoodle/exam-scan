# Software Dependencies Installation macOS

This is a step-by-step guide to download and install the various software dependencies of the exam scan program for macOS. This version of code was tested on macOS 10.14 Mojave

1. Install Homebrew

    Type the following command in Terminal:

    `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`

    Once you have installed Homebrew, insert the Homebrew directory at the top of your PATH environment variable. You can do this by adding the following line at the bottom of your ~/.profile file

    `export PATH="/usr/local/opt/python/libexec/bin:$PATH"`

    If you have OS X 10.12 (Sierra) or older use this line instead

    `export PATH=/usr/local/bin:/usr/local/sbin:$PATH`

    For more details refer to: [https://brew.sh/#install]

2. Install Python

    MacOS X comes with pre-installed Python. However its' version is lower (most likely Python 2.x). The code for exam-scan requires Python 3.8/3.9, therefore one has to upgrade.

    Python can be upgraded easily with Homebrew. Type this below command to install Python:

    `brew install python`

    After the installation type `python --version` and `python3 --version`. Both commands launch the Homebrew-installed Python 3 interpreter and should return the same python version (see below image).

    <img src="/assets/images/macpython.png" height="75" width ="500"/>

    For more details refer to this documentation: [https://docs.python-guide.org/starting/install3/osx/]

3. Install Imagemagick

    Type the following command in Terminal:

    `brew install imagemagick`

    For more details refer to this documentation: [https://docs.wand-py.org/en/latest/guide/install.html]

4. Install Ghostscripts

    Type the following command in Terminal:

    `brew install ghostscript`

5. Check pip

    The Python installation with Homebrew installs PIP as well. You can check if PIP is installed with the below command:

    `pip -V`

    The command returns the current version of pip installed in your system along with it's location. The above command works similarly if you substituted `pip` with `pip3`

    <img src="/assets/images/macpip.png" height="75" width ="500"/>

6. Install additional Python modules with pip

    Install additional python modules by typing the below command in command prompt:

    `pip install -r requirements.txt`

    The above command works similarly if you substituted `pip` with `pip3`.

## Note

To run the programs of exam scan you can use both `python3` and `python`command variations.
