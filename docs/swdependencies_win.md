# Software Dependencies Installation Windows

This is a step-by-step guide to download and install the various software dependencies of the exam scan program for Windows 10

1. Install Chocolatey

    Chocolatey is a package manager for Windows. It will help to install the different softwares the exam scan is dependant on.

    Please note: If you already have Python3, ImageMagick and Ghostscript installed, you do not need Chocolatey to run the exam scan scripts. Please continue reading at Step 6.

    Open the command prompt as Administrator and run the below command to install Chocolatey:

    ```bash
    @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command " [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
    ```

    For more details regarding Chocolatey installation, check this link: [https://chocolatey.org/docs/installation]

2. Install Python

    This version of code requires Python version 3.8 or 3.9. There are 2 ways by which one can install python:

    * Microsoft Store
    * Python Installer from Python.org

    * Microsft Store Method

    Go to Command Line and type `python` or `python3`. If python is installed in your system, python will be open in interactive mode, you can exit from it with command `quit()`. If python is not installed, the command will redirect to the Microsoft Store from where you can download and install. Check if the version number is 3.8.x or 3.9.x

    <img src="/assets/images/python installation4.png" float = "center" height="300" width ="500"/>

    * Python Installer from Python.org

        * Click on 3.8/3.9 stable python release from [https://www.python.org/downloads/windows/]
        * Select and download the executable installer according to your processor type (32-bit or 64-bit)
        * Double-click on the installer file and check the `Add Python <version> to PATH`. (This will add the Python path to Environmental Variables and that will help to run python programs from anywhere in your system.) You can choose to install Python in the default path, which is indicated by the red arrow; or  install it in your own chosen path by choosing 'Customize installation'.

        <img src="/assets/images/python installation.png" float = "center" height="300" width ="500"/>

        * Click Next and continue with the rest of the installation.
        * Once installation is complete, click 'Finish' to complete installation
        * Open command prompt and type `python --version` and verify the version.

3. Install Imagemagick

    Open the command prompt as Administrator and run the below command to install Imagemagick through Chocolatey:

    `choco install imagemagick.app`

    For more details refer to this documentation: [https://docs.wand-py.org/en/latest/guide/install.html]

4. Install Ghostscript

    Open the command prompt as Administrator and run the below command to install Imagemagick through Chocolatey:

    `choco install ghostscript`

5. Check or Install pip

    After python installation, one needs to verify python commands. Type `python` and `python3` in command prompt. The command that opens python in interactive mode(See picture below) is the command for you. It can also happen that both command works, that means you can use both commands.

    *Hint:* This version of code is tested with `Python 3.8` and `Python 3.9`. When installed from Microsoft Store both commands work and when installed throu.gh Python 3.8/3.9 executable installer from Python.org `python3` doesnt work.

    Ideally the python installation in Step 2 should install pip. Based on whether `python`or `python3` works for you, you can check if pip is installed by typing the command `pip -v` or `pip3 -v` respectively. If it is installed, it will list it's various functions. In case pip is not installed, install pip from: [https://pip.pypa.io/en/stable/installing/]

    <img src="/assets/images/pip.png" height="500" width ="500"/>

6. Install additional Python modules with pip

    Install additional python modules by typing the below command in command prompt:

    `pip install -r requirements.txt`

    The above command works similarly if you substituted `pip` with `pip3`.

    Now everything should be set up.

## Note

To run the programs of exam scan you can use

* `python3` command variations if `python3` and `pip3` works for you.
* `python` command variations  if `python` and `pip` works for you.
