#!/usr/bin/env python

"""Extract student's submission files from Moodle assignment or Dynexite exam

Transfer PDF files from ZIP file containing all submissions of a Moodle
assignment  or Dynexite file-upload task into output folder with file names
following exam scan naming convention.

Attention: Contents in output folder will be overwritten in the following!
"""

__author__ = "Amrita Deb Dutta(deb.dutta@itc.rwth-aachen.de), " +\
    "Christian Rohlfing (rohlfing@ient.rwth-aachen.de)"


import sys  # get arguments from command line
import os  # path listing/manipulation/...
import time  # keep track of time
import argparse  # handle command line arguments
import shutil  # unzipping and copying files

from utils import moodle as moodle


def dynexite_convert_images_pdf(files, output_dir, dpi, quality):
    import watermark

    # Iterate over all image files and convert them each to a single-page pdf
    pdf_files = []
    for img_file in files:
        if not img_file.lower().endswith('.pdf'):
            pdf_file = dynexite_convert_image_pdf(img_file, dpi, quality)
        else:
            pdf_file = img_file   # nothing to convert, skip
        pdf_files.append(pdf_file)

    # Combining PDFs into one PDF
    pdf_files.sort()
    combined_pdf_file = watermark.combine_all_pdfs(
        pdf_files, output_dir, combined_postfix='_merged')

    # Remove single-page PDFs
    for pdf_file in pdf_files:
        os.remove(pdf_file)

    return combined_pdf_file


def dynexite_convert_image_pdf(img_file, dpi, quality):
    """Convert single image to single PDF"""

    from PIL import Image  # Image handling
    import watermark

    image = Image.open(img_file).convert('RGBA')
    image = watermark.remove_transparency(image)
    image = image.convert('RGB')
    pdf_file = os.path.splitext(img_file)[0] + '.pdf'
    image.save(pdf_file, resolution=dpi, quality=quality)
    return pdf_file


def _make_parser():
    csv_parser = moodle.get_moodle_csv_parser()

    parser = argparse.ArgumentParser(
        parents=[csv_parser],
        description=__doc__, prog='importsubmissions.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "inzip", help="Input ZIP file or extracted folder.")
    parser.add_argument(
        "csv", help="Moodle grading sheet or RWTH exam registration sheet.")
    parser.add_argument(
        "outfolder", help="Output folder with PDFs.")
    parser.add_argument(
        "--moodle", default="1",
        help="Is the script being run with Moodle 4 data or earlier. 1 means it is Moodle4.x. 0 means older")
    parser.add_argument(
        "-x", "--dynexite", action='store_true',
        help="If set, expects data exported from Dynexite instead of moodle")
    parser.add_argument(
        "-f", "--filenameformat", default="{matnum}_{fullname[0]}",
        help="File name format. Available keywords: " +
        "{matnum}, {fullname}, {lastname}, {firstname}. " +
        "Default: '{matnum}_{fullname[0]}'")
    parser.add_argument(
        "-c", "--copyall", action='store_true',
        help="If set, copies all files (including multiple and non-PDF files)")
    parser.add_argument(
        "-a", "--appendoriginal", action='store_true',
        help="If set, appends original file name to new location's file name")
    parser.add_argument(
        "-d", "--dry", action='store_true', help="Flag for dry run.")
    parser.add_argument(
        "-t", "--tmp", default="./tmp", help="Temporary folder.")
    parser.add_argument(
        "--dpi", default="150",
        help="Dots-per-inch parameter for image to PDF conversion")
    parser.add_argument(
        "--quality", default="75",
        help="JPEG quality parameter for image to PDF conversion")

    return parser


# Create argument parser with default values
_parser = _make_parser()


def main(args):
    """Main routine

    1) Files are extracted from zip file location eg: ./all_submissions.zip
    2) Scan through extracted folder for PDF files.
    3) Matriculation number and last name are fetched from grading worksheet
    4) PDFs from extracted folder are renamed according to convention and
       placed in user provided outfolder
    """

    # Argument handling
    args = _parser.parse_args(args)
    inzip = args.inzip
    outfolder = args.outfolder
    sheet_csv = args.csv
    dry = args.dry
    csv_enc = args.csvenc
    csv_delim = args.csvdelim
    csv_quote = args.csvquote
    copy_all = args.copyall
    dynexite = args.dynexite
    append_original_name = args.appendoriginal
    filenameformat = args.filenameformat
    tmp_folder = args.tmp
    dpi = int(args.dpi)
    quality = int(args.quality)
    extracted_folder = os.path.join(tmp_folder, "extracted_from_moodle")
    moodle4 = int(str(args.moodle))

    # Check folders
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)

    # Print status
    starttime = time.time()
    num_students = moodle.get_student_number(sheet_csv=sheet_csv,
                                             csv_enc=csv_enc)

    print('''Preparing for renaming of submission files
Processing {} students
  '''.format(num_students))
    # Clean up and create temporary folder
    dryout = []
    if dry:
        print("Dry run\n")

    # Check whether zip or folder is given
    folder_instead_of_zip = False
    if not(inzip.lower().endswith(('.zip'))):
        if not(os.path.isdir(inzip)):
            raise Exception(
                "{zip} neither Zip file nor folder. Exiting."
                .format(zip=inzip))
        else:
            # Folder was given instead of Zip file
            extracted_folder = inzip
            folder_instead_of_zip = True
    else:
        # Extract
        if os.path.isfile(inzip):
            print("Extracting files from {zip} ...".format(zip=inzip))
            if not dry:
                shutil.unpack_archive(inzip, extracted_folder)  # unzip file
            else:
                raise Exception("Dry run prevents from unpacking Zip.")
        else:
            print("""
There are no zip files in the given directory.
Exiting now.""")
            return

    # List all extracted folders
    folders = os.listdir(extracted_folder)
    folders.sort()

    # Filter out dynexite protocol files
    if dynexite:
        folders = [_ for _ in folders if not _.lower().endswith('.pdf')]

    print('Found {} submissions'.format(len(folders)))

    # There should never be more folders than entries in CSV file
    if len(folders) > num_students and not(copy_all):
        raise Exception(
            ("More folders ({num_folders}) than "
             "students in CSV file ({num_students})")
            .format(num_folders=len(folders), num_students=num_students))

    # Parse grading infos from CSV file
    infos = moodle.extract_info(sheet_csv=sheet_csv, csv_delim=csv_delim,
                                csv_quote=csv_quote, csv_enc=csv_enc,
                                dynexite=dynexite)
    # Collect non-default cases:
    # Student did not submit anything
    infos_no_submission = []
    # Student submitted more than one file
    infos_multi_files = []
    # Student submitted a non-PDF file
    infos_unsupported_files = []

    # Loop over grading info
    print("Copying submissions", sep=' ', end='', flush=True)
    for cnt, info in enumerate(infos):
        # Create expected folder name from submission info
        folder = moodle.submission_folder_name(info, dynexite=dynexite, newmoodle=moodle4)
        if dynexite:  # dynexite has a different (kind of unpredictable folder
            # structure. However, folder starts with "matnum-")
            # Search for files starting with expected folder name
            folders_tmp = [f for f in folders if f.startswith(folder)]
            if len(folders_tmp) == 1:
                folder = folders_tmp[0]

        # Two possibilities: Either zip contains folders or files
        if folder in folders:  # Folder was found
            folderfull = os.path.join(extracted_folder, folder)
            files = os.listdir(folderfull)
        else:   # Search for files instead of folder
            # Search for files starting with expected folder name
            files = [f for f in folders if f.startswith(folder)]
            folderfull = extracted_folder

        # Notify if folder empty
        if len(files) == 0:
            infos_no_submission.append(info)

        # Notify if more than one submission
        if len(files) > 1:
            infos_multi_files.append(info)

            if dynexite:  # Todo: remove this constraint
                # Todo: This functionality could be helpfull not only for
                # dynexite submissions with images (created with telescope)
                # but also in the case if multiple PDFs were submitted with
                # Moodle

                # Check for images with file extensions
                exts = [os.path.splitext(f)[1].lower() for f in files]

                if '.jpg' in exts or '.png' in exts or '.jpeg' in exts:
                    files_full = [os.path.join(folderfull, _) for _ in files]
                    merged_pdf = dynexite_convert_images_pdf(
                        files=files_full, output_dir=folderfull,
                        dpi=dpi, quality=quality)
                    files = [os.path.basename(merged_pdf)]

        # Iterate over all files within folder
        for file_cnt, file in enumerate(files):
            file_full = os.path.join(folderfull, file)

            # Create destination file name
            dest = filenameformat.format(
                matnum=info['matnum'], fullname=info['fullname'],
                lastname=info['lastname'], firstname=info['firstname'])

            # Add unique file ID (only for copy all)
            if copy_all:
                dest = dest + "_{:03d}".format(file_cnt)

            base, ext = os.path.splitext(file)
            # Add original file name
            if append_original_name:
                dest = dest + "_" + base
            # Add extension
            dest = dest + ext
            dest_full = os.path.join(outfolder, dest)

            # Notify if non-PDF file
            is_pdf = file_full.lower().endswith('.pdf')
            if not is_pdf and \
                    info not in infos_unsupported_files:
                infos_unsupported_files.append(info)

            # Copy either first PDF file or all files if copyall is active
            if (file_cnt == 0 and is_pdf) or copy_all:
                if not dry:
                    shutil.copyfile(file_full, dest_full)
                else:
                    dryout.append(
                        "- {old} -> {new}"
                        .format(old=os.path.join(folder, file), new=dest))

        # Print for-loop progress
        if not (cnt % max(1, round(num_students/10))):
            print(".", sep=' ', end='', flush=True)

    print("done.")

    # Report back special cases
    for report in [(infos_no_submission, "no files"),
                   (infos_multi_files, "multiple files"),
                   (infos_unsupported_files, "unsupported files")]:
        infos, reason = report
        if len(infos) > 0:
            lines = ["- {folder} ({matnum})"
                     .format(folder=moodle.submission_folder_name(
                         _, dynexite=dynexite), matnum=_['matnum'])
                     for _ in infos]
            lines.sort()
            print(
                "\nSubmissions of {reason}:\n{lines}"
                .format(reason=reason, lines="\n".join(lines)))

    # Dry run output
    if not dry:
        # Delete temporary folder
        if not folder_instead_of_zip:
            shutil.rmtree(extracted_folder)
    else:
        dryout.sort()
        print("\nDry run results:\n{}".format("\n".join(dryout)))

    # Print status
    endtime = time.time()
    print("Time taken: {:.2f}".format(endtime-starttime))


if __name__ == '__main__':
    main(sys.argv[1:])
