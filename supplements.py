#!/usr/bin/env python

"""Prepare supplement material

This script copies and renames supplementary material (such as exam sheet or
sample solution) with file names following the exam scan naming convention.

This information is either taken from the filenames of exam scan PDFs or from
the Moodle grading CSV file.

Attention: Contents in output folder will be overwritten in the following!
"""

__author__ = "Amrita Deb Dutta(deb.dutta@itc.rwth-aachen.de), " +\
    "Christian Rohlfing (rohlfing@ient.rwth-aachen.de)"


import sys  # get arguments from command line
import os  # path listing/manipulation/...
import time  # keep track of time
import argparse  # handle command line arguments
import shutil  # copy

import utils.matnum as matnum_utils
import utils.moodle as moodle


def copy_supplements(supp_dir, supp_files, prefixes, output_dir, dry=False):
    """Copy supplement files

    Args:
        supp_dir (str): path to supplement folder
        output_dir (str): path to output folder
        prefixes (list): list of prefixes
        dry (bool): indicate dry run
    """

    dryout = []
    if dry:
        print("Dry run\n")
    else:
        print("Start renaming...", sep='', end='', flush=True)

    # Iterate over supplement files
    cnt = 0
    num_files = len(supp_files)*len(prefixes)
    copied_files = []
    for supp_file in supp_files:
        supp_filefull = os.path.join(supp_dir, supp_file)
        supp_stem = os.path.splitext(supp_file)[0]  # filename without .pdf

        # Iterate over scanned PDF files
        for prefix in prefixes:
            new_file = prefix + "_" + supp_stem + ".pdf"
            new_filefull = os.path.join(output_dir, new_file)

            # Copy
            if not dry:
                shutil.copyfile(supp_filefull, new_filefull)
            else:
                dryout.append(
                    "- {old} -> {new}"
                    .format(old=supp_file, new=new_file))
            copied_files.append(new_file)

            # Print progress
            if not (cnt % max(1, round(num_files/10))):
                print(".", sep=' ', end='', flush=True)
            cnt += 1

    # Display dry run results
    if dry:
        dryout.sort()
        print("\nDry run results:\n{}".format("\n".join(dryout)))
    else:
        print("done")

    return copied_files


def _make_parser():
    csv_parser = moodle.get_moodle_csv_parser()

    parser = argparse.ArgumentParser(
        parents=[csv_parser],
        description=__doc__, prog='supplements.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "infolder", help="Folder with supplements.")
    parser.add_argument(
        "prefix", help="Provides information to construct prefixes. Either " +
        "folder with scanned PDFs or Moodle grading CSV file.")
    parser.add_argument(
        "outfolder",
        help="Output folder with supplements per student.")

    parser.add_argument(
        "--filenameformat", default="{matnum}_{fullname[0]}",
        help="File name format. Available keywords: " +
        "{{matnum}}, {{fullname}}, {{lastname}}, {{firstname}}. " +
        "Default: '{{matnum}}_{{fullname[0]}}'")
    parser.add_argument(
        "-d", "--dry", action='store_true', help="Flag for dry run")

    return parser


# Create argument parser with default values
_parser = _make_parser()


def main(args):
    """Main routine
    """

    # Argument handling
    args = _parser.parse_args(args)
    supp_dir = args.infolder
    prefixinfo = args.prefix
    prefixformat = args.filenameformat
    output_dir = args.outfolder
    csv_enc = args.csvenc
    csv_delim = args.csvdelim
    csv_quote = args.csvquote
    dry = args.dry

    starttime = time.time()
    # Check folders
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Decide whether PDF folder or CSV file was given
    csvfilename = pdf_dir = ""
    ext = os.path.splitext(prefixinfo)[1].lower()
    if ext == '.csv':  # CSV file
        csvfilename = prefixinfo
        if not os.path.isfile(csvfilename):
            raise Exception("File {} does not exist.".format(csvfilename))
    elif ext == '':  # Folder
        pdf_dir = prefixinfo
        if not os.path.isdir(pdf_dir):
            raise Exception("Folder {} does not exist.".format(pdf_dir))
    else:
        raise Exception("{} neither CSV file nor folder.".format(prefixinfo))

    # Print status
    
    supp_folder = os.listdir(supp_dir)
    supp_files = [_ for _ in supp_folder if _.lower().endswith(".pdf")]
    if len(supp_files) > 0:
        print("""
Available supplement PDFs to be copied:
- {}

Files in output folder {} will be overwritten during this process.
    """.format("\n- ".join(supp_files),  output_dir))
    else:
        print("""
There are no PDFs in the given directory.
Exiting now.""")
        return

    # Create prefixes
    print(pdf_dir)
    if pdf_dir != "":  # Take prefixes from pdf directory
        pdf_folder = os.listdir(pdf_dir)
        pdf_files = [_ for _ in pdf_folder
                     if _.lower().endswith(".pdf") and
                     matnum_utils.starts_with_matnum(_)]
        prefixes = []
        for pdf_file in pdf_files:
            prefix = os.path.splitext(pdf_file)[0]  # take file name as prefix
            prefixes.append(prefix)
    else:  # Take prefixes from CSV file
        prefixes = []
        infos = moodle.extract_info(sheet_csv=csvfilename, csv_delim=csv_delim,
                                    csv_quote=csv_quote, csv_enc=csv_enc)
        for info in infos:
            prefix = prefixformat.format(
                matnum=info['matnum'], fullname=info['fullname'],
                lastname=info['lastname'], firstname=info['firstname'])
            prefixes.append(prefix)  # save prefix

    # Copy supplements to output dir and prepend prefixes
    copied_files = copy_supplements(supp_dir, supp_files, prefixes,
                                    output_dir, dry)

    # Print status
    endtime = time.time()
    print("""
All PDFs are renamed and can be found in {} folder.
Time taken: {:.2f}s
    """.format(output_dir, endtime-starttime))

    return copied_files


if __name__ == '__main__':
    main(sys.argv[1:])
