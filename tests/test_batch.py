import unittest
import time
import os
import tempfile
import shutil


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))
    
    def test_batch_newmoodle_with_zip(self):
        import batch

        expected_files = [
            'moodle_feedbacks.zip',
            'passwords.csv']
        expected_folders = [
            'Deb, Amrita_709_assignsubmission_file',
            'L2PExtra, L2PExtra_707_assignsubmission_file',
            'L2PStudent, L2PStudent_708_assignsubmission_file'
        ]
        expected_pdfs = [
            '353621_D_w_aes.pdf'
        ]

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        csv = './tests/assets/grades.csv'
        dpi = 50

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        zipout_dir = os.path.join(self.test_dir, 'zipout')
        os.mkdir(zipout_dir)

        # Copy supplements file
        batch.main([
            in_dir, csv, out_dir,
            "-t", tmp_dir, "--cores", "1", "--dpi", str(dpi),
            "--zip", "./tests/assets/newmoodle/submissions-moodle.zip", "--moodle", "1"])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

        # Unpack zip
        zipfullfile = os.path.join(out_dir, 'moodle_feedbacks.zip')
        shutil.unpack_archive(zipfullfile, zipout_dir)

        # Assert zip output
        created_folders = os.listdir(zipout_dir)
        created_folders.sort()
        self.assertEqual(expected_folders, created_folders)

        created_pdfs = os.listdir(os.path.join(zipout_dir, created_folders[0]))
        self.assertEqual(created_pdfs, expected_pdfs)
    
    def test_batch_newmoodle_with_scanned_files(self):
        import batch

        expected_files = [
            'moodle_feedbacks.zip',
            'passwords.csv']
        expected_folders = [
            'Deb, Amrita_709_assignsubmission_file',
            'L2PExtra, L2PExtra_707_assignsubmission_file',
            'L2PStudent, L2PStudent_708_assignsubmission_file'
        ]
        expected_pdfs = [
            '353621_D_w_aes.pdf'
        ]

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        csv = './tests/assets/grades.csv'
        dpi = 50

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        zipout_dir = os.path.join(self.test_dir, 'zipout')
        os.mkdir(zipout_dir)

        # Copy supplements file
        batch.main([
            in_dir, csv, out_dir,
            "-t", tmp_dir, "--cores", "1", "--dpi", str(dpi),
            "--zip", "./tests/assets/newmoodle/submissions-moodle.zip", "--moodle", "1"])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

        # Unpack zip
        zipfullfile = os.path.join(out_dir, 'moodle_feedbacks.zip')
        shutil.unpack_archive(zipfullfile, zipout_dir)

        # Assert zip output
        created_folders = os.listdir(zipout_dir)
        created_folders.sort()
        self.assertEqual(expected_folders, created_folders)

        created_pdfs = os.listdir(os.path.join(zipout_dir, created_folders[0]))
        self.assertEqual(created_pdfs, expected_pdfs)
