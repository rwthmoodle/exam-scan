import unittest
import time
import os
import tempfile


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_newmoodle_zip_with_folders(self):
        import importsubmissions

        expected_files = [
            '234567_L.pdf',
            '353621_D.pdf',
            '458962_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions-moodle-folders.zip'
        sheet_csv = "./tests/assets/grades.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Call function
        importsubmissions.main([
            in_zip, sheet_csv, out_dir, "-t", tmp_dir])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    
    def test_newmoodle_folder_with_folders(self):
        import importsubmissions
        import shutil  # unzipping and copying files

        expected_files = [
            '234567_L.pdf',
            '353621_D.pdf',
            '458962_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions-moodle-folders.zip'
        sheet_csv = "./tests/assets/grades.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Extract zip
        # This may take a lot of time for larger exams
        extracted_dir = os.path.join(self.test_dir, 'extracted')
        os.mkdir(extracted_dir)

        shutil.unpack_archive(in_zip, extracted_dir)

        # Call function
        # with the already extracted folder instead of the zip file
        importsubmissions.main([
            extracted_dir, sheet_csv, out_dir, "-t", tmp_dir])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    
    def test_newmoodle_zip_with_files(self):
        import importsubmissions

        expected_files = [
            '234567_L.pdf',
            '353621_D.pdf',
            '458962_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions-moodle-folders.zip'
        sheet_csv = "./tests/assets/grades.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Call function
        importsubmissions.main([
            in_zip, sheet_csv, out_dir, "-t", tmp_dir])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)


    def test_dynexite_zip_with_folders(self):
        import importsubmissions

        expected_files = [
            '123001_L.pdf',
            '123002_L.pdf',
            '123010_L.pdf',
            '123011_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions_dynexite.zip'
        sheet_csv = "./tests/assets/RWTHonline.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Call function
        importsubmissions.main([
            in_zip, sheet_csv, out_dir, "-t", tmp_dir, "--dynexite",
            "--csvdelim", ";", "--csvenc", "ISO-8859-1", "--moodle", "0"])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    def test_dynexite_folder_with_folders(self):
        import importsubmissions
        import shutil  # unzipping and copying files

        expected_files = [
            '123001_L.pdf',
            '123002_L.pdf',
            '123010_L.pdf',
            '123011_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions_dynexite.zip'
        sheet_csv = "./tests/assets/RWTHonline.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Extract zip
        # This may take a lot of time for larger exams
        extracted_dir = os.path.join(self.test_dir, 'extracted')
        os.mkdir(extracted_dir)

        shutil.unpack_archive(in_zip, extracted_dir)

        # Call function
        # with the already extracted folder instead of the zip file
        importsubmissions.main([
            extracted_dir, sheet_csv, out_dir, "-t", tmp_dir, "--dynexite",
            "--csvdelim", ";", "--csvenc", "ISO-8859-1"])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    def test_dynexite_folder_with_folders_images(self):
        import importsubmissions
        import shutil  # unzipping and copying files
        import watermark

        expected_files = [
            '123001_L.pdf',
            '123002_L.pdf',
            '123010_L.pdf',
            '123011_L.pdf']

        # Prepare parameter
        in_zip = './tests/assets/submissions_dynexite.zip'
        sheet_csv = "./tests/assets/RWTHonline.csv"
        dpi = 150

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        tmp_img_dir = os.path.join(self.test_dir, 'tmp_img')
        os.mkdir(tmp_img_dir)

        # Extract zip
        # This may take a lot of time for larger exams
        extracted_dir = os.path.join(self.test_dir, 'extracted')
        os.mkdir(extracted_dir)

        shutil.unpack_archive(in_zip, extracted_dir)

        # Extract images from one PDF
        folders = [
            _ for _ in os.listdir(extracted_dir)
            if not _.lower().endswith('.pdf')]
        folders.sort()
        first_folder = folders[0]
        in_img_dir = os.path.join(extracted_dir, first_folder)
        first_pdf = os.listdir(in_img_dir)[0]
        watermark.convert_pdf_to_img(
            pdf_file=first_pdf, input_dir=in_img_dir,
            tmp_dir=tmp_img_dir, dpi=dpi, img_ext='jpg')
        # Remove PDF
        os.remove(os.path.join(in_img_dir, first_pdf))

        # Copy images to folder
        for file in os.listdir(tmp_img_dir):
            path_src = os.path.join(tmp_img_dir, file)
            path_dest = os.path.join(in_img_dir, file)
            shutil.copyfile(path_src, path_dest)

        # Call function
        # with the already extracted folder instead of the zip file
        importsubmissions.main([
            extracted_dir, sheet_csv, out_dir, "-t", tmp_dir, "--dynexite",
            "--csvdelim", ";", "--csvenc", "ISO-8859-1"])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)
