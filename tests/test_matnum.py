import unittest
import time


class MainTest(unittest.TestCase):
    expected_matnums = [  # either 6 or 5 digits
        "123456",
        "12345"
    ]

    valid_cases = [
        "123456_lastname_firstname_something_else_scanned_20220209.pdf",
        "12345_lastname_firstname_something_else_scanned_20220209.pdf",
        "123456.pdf",
        "12345.pdf",
    ]

    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_starts_with(self):
        from utils import matnum as matnum_utils

        for s in self.valid_cases:
            res = matnum_utils.starts_with_matnum(s)
            self.assertTrue(res)

    def test_get_matnum(self):
        from utils import matnum as matnum_utils

        for s in self.valid_cases:
            matnum = matnum_utils.get_matnum(s)
            self.assertTrue(matnum in self.expected_matnums)
