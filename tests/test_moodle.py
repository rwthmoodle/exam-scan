import unittest
import time


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_extract_grading_info_moodle(self):
        from utils import moodle as moodle_utils

        csv_file = "./tests/assets/grades.csv"
        gis = moodle_utils.extract_info_moodle(sheet_csv=csv_file)
        self.assertEqual(gis[2]['lastname'], "Deb")

        num_students = moodle_utils.get_student_number(sheet_csv=csv_file)
        self.assertEqual(num_students, 3)

        csv_file = "./tests/assets/grades.csv"
        gis = moodle_utils.extract_info_moodle(sheet_csv=csv_file)

        # Check that "d'Lastname" gets whitened to "dLastname"
        self.assertEqual(gis[0]['lastname'], "L2PStudent")

    def test_extract_grading_info_rwth_online(self):
        from utils import moodle as moodle_utils

        csv_file = "./tests/assets/RWTHonline.csv"
        gis = moodle_utils.extract_info_rwth_online(sheet_csv=csv_file)
        self.assertEqual(gis[2]['lastname'], "LastnameJ")

        num_students = moodle_utils.get_student_number(sheet_csv=csv_file)
        self.assertEqual(num_students, 5)

        csv_file = "./tests/assets/RWTHonline.csv"
        gis = moodle_utils.extract_info_rwth_online(sheet_csv=csv_file)

        # Check that "d'Lastname" gets whitened to "dLastname"
        self.assertEqual(gis[0]['lastname'], "dLastname")
