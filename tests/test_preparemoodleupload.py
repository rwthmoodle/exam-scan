import unittest
import time
import os
import tempfile
import shutil


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

        # Clean up
        shutil.rmtree(self.test_dir)

    def test_preparemoodleupload_single(self):
        import preparemoodleupload

        expected_feedback_folder =\
            'L2PStudent, L2PStudent_708_assignsubmission_file'
        expected_feedback_file = '234567_L.pdf'

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        sheet_csv = "./tests/assets/grades.csv"
        feedback_zip = 'feedbacks.zip'

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        out_zip = os.path.join(self.test_dir, feedback_zip)

        # Call function
        preparemoodleupload.main([in_dir, sheet_csv, out_zip,
                            "-t", tmp_dir])

        # Unpack feedbacks
        shutil.unpack_archive(out_zip, tmp_dir)
        feedback_folders = os.listdir(tmp_dir)
        self.assertTrue(feedback_folders[0], expected_feedback_folder)

        feedbacks = os.listdir(os.path.join(tmp_dir, feedback_folders[0]))
        self.assertTrue(feedbacks[0], expected_feedback_file)
