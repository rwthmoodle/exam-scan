import unittest
import time


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_extract_first_qr(self):
        from utils import qr as qr_utils

        expected_qr = "23-16"

        pdf_file = "./tests/assets/qr_tests/pdfs/123001_LastnameA.pdf"
        qr = qr_utils.first_qr_from_first_pdf_page(pdf_file=pdf_file)
        self.assertEqual(qr, expected_qr)

    def test_extract_all_qrs(self):
        from utils import qr as qr_utils

        expected_qrs = [
            ['23-01'],
            ['23-02'],
            ['23-03'],
            ['23-04'],
            ['23-05'],
            ['23-06'],
            ['23-07'],
            ['23-08'],
            ['23-09'],
            ['23-10'],
            ['23-11'],
            ['23-12'],
            ['23-13'],
            ['23-14'],
            ['23-15'],
            ['23-16']
        ]

        # Parameters
        dpi = 200
        pdf_file = "./tests/assets/qr_tests/pdfs/123001_LastnameA.pdf"

        # Decode all QRs
        qrs, _ = qr_utils.qrs_from_pdf(pdf_file=pdf_file, dpi=dpi)
        qrs.sort()
        self.assertEqual(qrs, expected_qrs)
