import unittest
import time
import os
import tempfile


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_qr_exception(self):
        import renamescans

        # Prepare parameter
        in_dir = './tests/assets/pdfs_scan'
        sheet_csv = "./tests/assets/Grades.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        # Call function
        try:
            renamescans.main([
                in_dir, sheet_csv, out_dir,
                "--dry", "--checkqr"])
        except Exception:
            pass

    def test_rename_by_csv(self):
        import renamescans

        expected_files = [
            '123001_L.pdf',
            '123002_L.pdf',
            '123010_L.pdf',
            '123011_L.pdf',
            '123012_d.pdf']

        # Prepare parameter
        in_dir = './tests/assets/pdfs_scan'
        sheet_csv = "./tests/assets/grades_scans.csv"

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        # Call function
        renamescans.main([in_dir, sheet_csv, out_dir])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)
