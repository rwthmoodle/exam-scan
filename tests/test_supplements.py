import unittest
import time
import os
import tempfile


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly

        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

    def test_supplements_from_pdf_folder(self):
        import supplements

        expected_files = [
            '234567_L_GDET3_20H.pdf',
            '234567_L_GDET3_20H_loes.pdf',
            '353621_D_GDET3_20H.pdf',
            '353621_D_GDET3_20H_loes.pdf',
            '458962_L_GDET3_20H.pdf',
            '458962_L_GDET3_20H_loes.pdf']

        # Prepare parameter
        supp_dir = './tests/assets/supplements'
        pdf_dir = './tests/assets/pdfs'

        supp_out_dir = os.path.join(self.test_dir, 'supplements_out')
        os.mkdir(supp_out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Copy supplements file
        supplements.main([supp_dir, pdf_dir, supp_out_dir])

        # Assert output
        created_files = os.listdir(supp_out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    def test_supplements_from_csv(self):
        import supplements

        expected_files = [
            '234567_L_GDET3_20H.pdf',
            '234567_L_GDET3_20H_loes.pdf',
            '353621_D_GDET3_20H.pdf',
            '353621_D_GDET3_20H_loes.pdf',
            '458962_L_GDET3_20H.pdf',
            '458962_L_GDET3_20H_loes.pdf']

        # Prepare parameter
        supp_dir = './tests/assets/supplements'
        csv = './tests/assets/grades.csv'

        supp_out_dir = os.path.join(self.test_dir, 'supplements_out')
        os.mkdir(supp_out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        # Copy supplements file
        supplements.main([supp_dir, csv, supp_out_dir])

        # Assert output
        created_files = os.listdir(supp_out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

    def test_supplements_watermark(self):
        import supplements
        import watermark

        expected_files = [
            '234567_L_GDET3_20H_loes_w.pdf',
            '234567_L_GDET3_20H_w.pdf',
            '353621_D_GDET3_20H_loes_w.pdf',
            '353621_D_GDET3_20H_w.pdf',
            '458962_L_GDET3_20H_loes_w.pdf',
            '458962_L_GDET3_20H_w.pdf']

        # Prepare parameter
        supp_dir = './tests/assets/supplements'
        pdf_dir = './tests/assets/pdfs'
        dpi = 100

        supp_out_dir = os.path.join(self.test_dir, 'supplements_out')
        os.mkdir(supp_out_dir)

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        # Copy supplements file
        supplements.main([supp_dir, pdf_dir, supp_out_dir])

        # Watermark files
        watermark.main(
            [supp_out_dir, out_dir, "-t", tmp_dir, "--dpi", str(dpi)])

        # Assert output
        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)
