import unittest
import time
import os
import tempfile
import shutil


class MainTest(unittest.TestCase):
    def setUp(self):
        self.tic = time.time()  # todo this is sooo ugly
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        self.toc = time.time()
        t = self.toc - self.tic
        print('Time: %.3f' % (t))

        # Clean up
        shutil.rmtree(self.test_dir)

    def test_watermark_single_pdf(self):
        import watermark

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        dpi = 150
        quality = 75
        fontsize = 75
        pdf_file = '234567_L.pdf'

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        # Call function
        watermark.watermark_pdf(input_dir=in_dir, tmp_dir=tmp_dir,
                                output_dir=out_dir, fontsize=fontsize, dpi=dpi,
                                quality=quality, imgmgckdir=None, pdf_file=pdf_file)

        self.assertTrue(os.listdir(out_dir)[0], '234567_L_w.pdf')

    def test_watermark_pdfs(self):
        import watermark

        expected_files = ["234567_L_w.pdf",
                          "353621_D_w.pdf",
                          "458962_L_w.pdf"]

        # Prepare parameter
        in_dir = './tests/assets/pdfs'
        dpi = 150

        tmp_dir = os.path.join(self.test_dir, 'tmp')
        os.mkdir(tmp_dir)

        out_dir = os.path.join(self.test_dir, 'out')
        os.mkdir(out_dir)

        # Watermark files
        watermark.main([in_dir, out_dir,
                        "-t", tmp_dir, "--dpi", str(dpi)])

        created_files = os.listdir(out_dir)
        created_files.sort()
        self.assertEqual(expected_files, created_files)

