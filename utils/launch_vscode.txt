{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Current File",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            /* Arguments for Testing */
            /* IMPORT:new moodle: zip with folders */       //"args": ["./tests/assets/newmoodle/submissions-moodle.zip", "./tests/assets/newmoodle/grades.csv", "./tests/assets/newmoodle/SOLO/pdfs"],
            /* IMPORT:old moodle: zip with folders */      //"args": ["./tests/assets/submissions-moodle.zip", "./tests/assets/Grades.csv", "./tests/assets/oldmoodle/SOLO/pdfs", "--moodle", "0"],
            /* IMPORT:new moodle: zip w/o folders  */     //"args": ["./tests/assets/newmoodle/submission-moodle-files.zip", "./tests/assets/newmoodle/grades.csv", "./tests/assets/newmoodle/SOLO/pdfs"],
            /* IMPORT:old moodle: zip w/o folders  */    //"args": ["./tests/assets/submissions-moodle-files.zip", "./tests/assets/Grades.csv", "./tests/assets/oldmoodle/SOLO/pdfs", "--moodle", "0"],
            /* WATERMARK:new moodle */                  //"args": ["./tests/assets/newmoodle/SOLO/pdfs", "./tests/assets/newmoodle/SOLO/pdfs_watermarked", "--cores", "2", "--dpi", "150", "--quality", "75"],
            /* WATERMARK:old moodle */                 //"args": ["./tests/assets/oldmoodle/SOLO/pdfs", "./tests/assets/oldmoodle/SOLO/pdfs_watermarked", "--cores", "2", "--dpi", "150", "--quality", "75"],
            /* SUPPLEMENTS:new moodle */              //"args": ["./tests/assets/supplements", "./tests/assets/newmoodle/grades.csv", "./tests/assets/newmoodle/SOLO/pdfs"],
            /* SUPPLEMENTS:old moodle */             //"args": ["./tests/assets/supplements", "./tests/assets/Grades.csv", "./tests/assets/oldmoodle/SOLO/pdfs"], 
            /* ENCRYPT: new moodle */               //"args": ["./tests/assets/newmoodle/SOLO/pdfs_watermarked", "./tests/assets/newmoodle/SOLO/pdfs_encrypted", "--password", "ganzgeheim"], 
            /* ENCRYPT: old moodle */              //"args": ["./tests/assets/oldmoodle/SOLO/pdfs_watermarked", "./tests/assets/oldmoodle/SOLO/pdfs_encrypted", "--password", "ganzgeheim"],
            /* PREPARE: new moodle */             //"args": ["./tests/assets/newmoodle/SOLO/pdfs_encrypted", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/SOLO/out/feedback.zip"], 
            /* PREPARE: old moodle */            //"args": ["./tests/assets/oldmoodle/SOLO/pdfs_encrypted", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/SOLO/out/feedback.zip", "--moodle", "0"], 
            
            /* BATCH: newmoodle:Zip w/o folders:w/o supp   */      //"args": ["./tests/assets/newmoodle/BATCH/pdfs", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/newmoodle/submission-moodle-files.zip"], 
            /* BATCH: newmoodle:Zip with folders:w/o supp  */     //"args": ["./tests/assets/newmoodle/BATCH/pdfs", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/newmoodle/submissions-moodle.zip"], 
            /* BATCH: oldmoodle:Zip with folders:w/o supp  */    //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/submissions-moodle.zip", "--moodle", "0"], 
            /* BATCH: oldmoodle:Zip with folders:with supp */    //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/submissions-moodle.zip", "--moodle", "0", "--suppinfolder", "./tests/assets/supplements", "--supp"], 
            /* BATCH: newmoodle:Zip w/o folders:with supp  */    //"args": ["./tests/assets/newmoodle/BATCH/pdfs", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/newmoodle/submission-moodle-files.zip", "--suppinfolder", "./tests/assets/supplements", "--supp"], 
            /* BATCH: oldmoodle:Zip w/o folders:with supp  */   //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/submissions-moodle-files.zip", "--moodle", "0", "--suppinfolder", "./tests/assets/supplements", "--supp"], 
            /* BATCH: oldmoodle:Zip w/o folders:w/o supp   */    //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--zip", "./tests/assets/submissions-moodle-files.zip", "--moodle", "0"], //BATCH: oldmoodle:Zip w/o folders:w/o supp
            /* BATCH: newmoodle:scanned exams:wo supp      */    //"args": ["./tests/assets/newmoodle/BATCH/pdfs", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/BATCH/out", "--password", "ganzgeheim"], 
            /* BATCH: newmoodle:scanned exams:with supp    */    //"args": ["./tests/assets/newmoodle/BATCH/pdfs", "./tests/assets/newmoodle/grades.csv","./tests/assets/newmoodle/BATCH/out", "--password", "ganzgeheim", "--suppinfolder", "./tests/assets/supplements", "--supp"], 
            /* BATCH: oldmoodle:scanned exams:wo supp      */    //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--moodle", "0"], 
            /* BATCH: oldmoodle:scanned exams:with supp    */    //"args": ["./tests/assets/oldmoodle/BATCH/pdfs", "./tests/assets/Grades.csv","./tests/assets/oldmoodle/BATCH/out", "--password", "ganzgeheim", "--suppinfolder", "./tests/assets/supplements", "--supp", "--moodle", "0"], 
            //"args": [],
            "console": "integratedTerminal",
            "justMyCode": true
        }
    ]
}
