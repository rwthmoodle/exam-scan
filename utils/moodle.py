"""Common functions for handling Moodle grading sheet CSV
"""


import csv  # handles csv
import argparse  # argument parser for CSV files


def get_student_number(sheet_csv, csv_enc='utf-8'):
    """Count number of student entries in grading sheet

    Args:
        sheet_csv (str): filename of grading sheet CSV
        csv_enc (str): CSV encoding

    Returns:
        int: number of entries
    """

    # Open CSV file and count lines
    num_students = 0
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        num_students = sum(1 for _ in csvfile)

    num_students -= 1  # do not count header line

    return num_students


def submission_folder_name(grading_info, dynexite=False, newmoodle=1):
    """Create submission folder name

    Args:
        grading_info ([dict]): student's grading info from CSV

    Returns:
        str: submission folder name
    """
    if not dynexite:
        if newmoodle:
            template = "{fullname}_{moodleid}_assignsubmission_file"
        else:
            template = "{fullname}_{moodleid}_assignsubmission_file_"
    else:
        template = "{matnum}-"

    foldername = template.format(fullname=grading_info['fullname'],
                                 moodleid=grading_info['moodleid'],
                                 matnum=grading_info['matnum'])

    return foldername


def extract_info(sheet_csv, dynexite=False, csv_delim=',', csv_quote='"', csv_enc='utf-8'):

    if not dynexite:
        return extract_info_moodle(
            sheet_csv=sheet_csv, csv_delim=csv_delim, csv_quote=csv_quote,
            csv_enc=csv_enc)
    else:
        return extract_info_rwth_online(
            sheet_csv=sheet_csv, csv_delim=csv_delim, csv_quote=csv_quote,
            csv_enc=csv_enc)


def extract_info_moodle(sheet_csv, csv_delim=',', csv_quote='"', csv_enc='utf-8'):
    """Extract grading information from grading sheet

    Args:
        sheet_csv (str): filename of grading sheet CSV
        csv_delim (str, optional): CSV delimiter. Defaults to ','.
        csv_quote (str, optional): CSV quote char. Defaults to '"'.
        csv_enc (str, optional): CSV encoding. Defaults to 'utf-8'.
        Typical values: "'utf-8', 'utf-8-sig', or 'cp1252' (Windows). "

    Returns:
        list of dicts: grading information with following info per student:
        moodleid, fullname, matnum, lastname, firstname
    """

    # CSV header
    # We only parse these fieldnames, all other columns are listed under [None]
    fieldnames = ['longid', 'fullname', 'matnum', 'status', 'grade']

    # Check delimiter
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        try:
            csv.Sniffer().sniff(csvfile.read(1024),
                                delimiters=csv_delim)
        except csv.Error as error:
            print("csv_delim='{csvdelim}' not correct for file {csv}"
                  .format(csvdelim=csv_delim, csv=sheet_csv))
            raise ValueError(error)

    # Open CSV file
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        # Convert CSV to list of dicts
        reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                delimiter=csv_delim, quotechar=csv_quote)
        grading_infos = list(reader)  # convert to list
        grading_infos.pop(0)  # do not save header

        # Clean up Moodle ID and split fullname
        for gi in grading_infos:
            # Clean up Moodle ID
            # "Participant xxx" -> "xxx" or
            # "Teilnehmer/inxxx" -> "xxx"
            moodleid = gi['longid']
            moodleid = moodleid.replace("Teilnehmer/in", "")  # German
            moodleid = moodleid.replace("Participant ", "")  # English
            gi['moodleid'] = moodleid

            # Split up fullname into first and last name
            fullname = gi['fullname'].replace("'", "")  # remove "'"
            try:
                gi['lastname'], gi['firstname'] = fullname.split(", ")
            except ValueError:
                raise Exception(
                    "csv_delim='{csvdelim}' not correct for file {csv}"
                    .format(csvdelim=csv_delim, csv=sheet_csv))

            gi['fullname'] = fullname

    return grading_infos

def extract_info_rwth_online(sheet_csv, csv_delim=';', csv_quote='"', csv_enc='ISO-8859-1'):
    """Extract grading information from RWTHonline (registered students for exam)

    Args:
        sheet_csv (str): filename of exported CSV
        csv_delim (str, optional): CSV delimiter. Defaults to ','.
        csv_quote (str, optional): CSV quote char. Defaults to '"'.
        csv_enc (str, optional): CSV encoding. Defaults to 'ISO-8859-1'.
        Typical values: "'utf-8', 'utf-8-sig', or 'cp1252', 'ISO-8859-1' (Windows). "

    Returns:
        list of dicts: grading information with following info per student:
        fullname, matnum, lastname, firstname
    """

    # CSV header
    # We only parse these fieldnames, all other columns are listed under [None]
    fieldnames = [
        'STUDY_PROGRAMME', 'CODE_OF_STUDY_PROGRAMME',
        'Studienplan_Version', 'SPO_KONTEXT',
        'matnum', 'lastname', 'firstname',  # changes here
        'GESCHLECHT', 'DATE_OF_ASSESSMENT',
        'GUEL_U_AKTUELLE_ANTRITTE_SPO',
        'grade',  # change here
        'REMARK', 'Number_Of_The_Course', 'SEMESTER_OF_The_COURSE',
        'COURSE_TITLE', 'Examiner', 'Start_Time', 'TERMIN_ORT',
        'DB_Primary_Key_Of_Exam',
        'moodleid',  # change here
        'COURSE_GROUP_NAME',
        'status',  # change here
        'EMAIL_ADDRESS', 'ECTS_GRADE'
    ]

    # Check delimiter
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        try:
            csv.Sniffer().sniff(csvfile.read(1024),
                                delimiters=csv_delim)
        except csv.Error as error:
            print("csv_delim='{csvdelim}' not correct for file {csv}"
                  .format(csvdelim=csv_delim, csv=sheet_csv))
            raise ValueError(error)

    # Open CSV file
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        # Convert CSV to list of dicts
        reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                delimiter=csv_delim, quotechar=csv_quote)
        grading_infos = list(reader)  # convert to list
        grading_infos.pop(0)  # do not save header

        # Clean up Moodle ID and split fullname
        for gi in grading_infos:
            gi['lastname'] = gi['lastname'].replace("'", "")
            gi['firstname'] = gi['firstname'].replace("'", "")
            gi['fullname'] = "{}, {}".format(
                gi['lastname'], gi['firstname'])

    return grading_infos



def extract_info_rwth_online(sheet_csv, csv_delim=';', csv_quote='"', csv_enc='ISO-8859-1'):
    """Extract grading information from RWTHonline (registered students for exam)

    Args:
        sheet_csv (str): filename of exported CSV
        csv_delim (str, optional): CSV delimiter. Defaults to ','.
        csv_quote (str, optional): CSV quote char. Defaults to '"'.
        csv_enc (str, optional): CSV encoding. Defaults to 'ISO-8859-1'.
        Typical values: "'utf-8', 'utf-8-sig', or 'cp1252', 'ISO-8859-1' (Windows). "

    Returns:
        list of dicts: grading information with following info per student:
        fullname, matnum, lastname, firstname
    """

    # CSV header
    # We only parse these fieldnames, all other columns are listed under [None]
    fieldnames = [
        'STUDY_PROGRAMME', 'CODE_OF_STUDY_PROGRAMME',
        'Studienplan_Version', 'SPO_KONTEXT',
        'matnum', 'lastname', 'firstname',  # changes here
        'GESCHLECHT', 'DATE_OF_ASSESSMENT',
        'GUEL_U_AKTUELLE_ANTRITTE_SPO',
        'grade',  # change here
        'REMARK', 'Number_Of_The_Course', 'SEMESTER_OF_The_COURSE',
        'COURSE_TITLE', 'Examiner', 'Start_Time', 'TERMIN_ORT',
        'DB_Primary_Key_Of_Exam',
        'moodleid',  # change here
        'COURSE_GROUP_NAME',
        'status',  # change here
        'EMAIL_ADDRESS', 'ECTS_GRADE'
    ]

    # Check delimiter
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        try:
            csv.Sniffer().sniff(csvfile.read(1024),
                                delimiters=csv_delim)
        except csv.Error as error:
            print("csv_delim='{csvdelim}' not correct for file {csv}"
                  .format(csvdelim=csv_delim, csv=sheet_csv))
            raise ValueError(error)

    # Open CSV file
    with open(sheet_csv, newline='', encoding=csv_enc) as csvfile:
        # Convert CSV to list of dicts
        reader = csv.DictReader(csvfile, fieldnames=fieldnames,
                                delimiter=csv_delim, quotechar=csv_quote)
        grading_infos = list(reader)  # convert to list
        grading_infos.pop(0)  # do not save header

        # Clean up Moodle ID and split fullname
        for gi in grading_infos:
            gi['lastname'] = gi['lastname'].replace("'", "")
            gi['firstname'] = gi['firstname'].replace("'", "")
            gi['fullname'] = "{}, {}".format(
                gi['lastname'], gi['firstname'])

    return grading_infos


def get_info_by_matnum(infos, matnum):
    info = next(info for info in infos if info["matnum"] == matnum)

    if not info:
        raise Exception(
            "Matnum {matnum} not found in grading infos.".format(matnum=matnum)
        )
    return info


def get_moodle_csv_parser():
    csv_parser = argparse.ArgumentParser(add_help=False)
    csv_parser.add_argument(
        "--csvdelim", default=",", help="CSV delimiter character.")
    csv_parser.add_argument(
        "--csvquote", default='"', help="CSV quote character.")
    csv_parser.add_argument(
        "--csvenc", default="utf-8", help="CSV encoding scheme. " +
        "Typical encodings:'utf-8', 'utf-8-sig', or 'cp1252' (Windows).")

    return csv_parser
