#!/usr/bin/env python

"""Watermarks each page of PDF with matriculation number

PDFs of exam scans from input folder are watermarked with the matriculation
number of the respective student. Watermarked PDFs are stored in output folder

Attention: Contents in output folder will be overwritten in the following!
"""

__author__ = "Amrita Deb Dutta(deb.dutta@itc.rwth-aachen.de), " +\
    "Christian Rohlfing (rohlfing@ient.rwth-aachen.de), " +\
    "Rafael Goldbeck (rafael.goldbeck@isea.rwth-aachen.de)"


import sys  # get arguments from command line
import os  # path listing/manipulation/...
import re  # pattern matching for directory names
import time  # keep track of time
import argparse  # handle command line arguments
from multiprocessing import Pool  # multi processing
from functools import partial
from wand.image import Image as wi  # PDF to images
from PIL import Image, ImageDraw, ImageFont  # Image handling
from pikepdf import Pdf  # combining PDFs
from contextlib import contextmanager
from tempfile import TemporaryDirectory

import utils.matnum as matnum_utils


@contextmanager
def set_env(**environ):
    """
    Temporarily set the process environment variables.

    >>> with set_env(PLUGINS_DIR='test/plugins'):
    ...   "PLUGINS_DIR" in os.environ
    True

    >>> "PLUGINS_DIR" in os.environ
    False

    :type environ: dict[str, unicode]
    :param environ: Environment variables to set
    """
    old_environ = dict(os.environ)
    os.environ.update(environ)
    try:
        yield
    finally:
        os.environ.clear()
        os.environ.update(old_environ)


def convert_pdf_to_img(pdf_file, input_dir, tmp_dir, dpi, img_ext='png'):
    """Converts all pages from a PDF to single images

    PDF is located in input directory and single images are stored in temporary
    directory.

    Args:
    pdf_file (str):     path of pdf located in input directory
    input_dir (str):    path of input directory
    tmp_dir (str):      path of temporary directory
    dpi (int):          dots per inch for image conversion
    img_ext (str):      file format of images given by extension (png, jpg)

    Returns:
    img_files (list)    path of images stored in temporary directory
    """

    # Read in whole PDF
    pdf_path = os.path.join(input_dir, pdf_file)
    pdf = wi(filename=pdf_path, resolution=dpi)
    # Remove Imagemagick files
    
    # Iterate over pages and store them as image
    img_files = []
    for id, img in enumerate(pdf.sequence):
        image = wi(image=img)
        # Create image path
        tmp = "{base}_{id:03d}.{ext}".format(
            base=os.path.splitext(pdf_file)[0], id=id, ext=img_ext)
        img_file = os.path.join(tmp_dir, tmp)

        # Convert to Pillow
        image.save(filename=img_file)
        img_files.append(img_file)

    return img_files


def create_watermark_template(img_file, matnum, fontsize, dpi):
    """Creates transparent image with repeated matriculation number

    This function creates a transparent image with the student's matriculation
    number repeated throughout the image. This image will be used as the
    watermark.

    Args:
        img_file (str): path of image
        matnum (str): matriculation number
        dpi (int): dots per inch

    Returns:
        PIL.Image.Image: transparent image containing matriculation number of
        student repeated throughout
    """

    image = Image.open(img_file).convert('RGBA')
    width, height = image.size

    # Positions of watermarks
    x_pos = []
    y_pos = []
    x_dist = int(2*dpi)
    y_dist = int(2*dpi)
    for y in range(0, height*2, x_dist):
        for x in range(0, width*2, y_dist):
            x_pos.append(x)
            y_pos.append(y)

    # Blank image for text, initialized to transparent background color
    newsize = tuple(2*x for x in image.size)
    template = Image.new('RGBA', newsize, (255, 255, 255, 0))

    # Font
    fnt = ImageFont.truetype(
        './assets/fonts/arial.ttf', round(fontsize*dpi/250))

    # Drawing context
    d = ImageDraw.Draw(template)

    # Draw text at half opacity
    for i in range(len(x_pos)):
        d.text((x_pos[i], y_pos[i]), matnum, font=fnt,
               fill=(149, 149, 151, 100))

    # Rotate template
    template = template.rotate(340, expand=1)

    return template


def remove_transparency(im, bg_colour=(255, 255, 255)):
    """
    Correct transparent image turning black issue

    Args:
        im (PIL.Image.Image): pdf page image
        bg_colour (tuple): background color white code

    Returns:
        PIL.Image.Image: corrected image when the image is transparent
        else just return the pdf page image
    """

    if (im.mode in ('RGBA', 'LA')) or (im.mode == 'P' and
                                       'transparency' in im.info):
        alpha = im.convert('RGBA').split()[-1]
        # Create a new background image of our matt color.
        # Must be RGBA because paste requires both images have the same format
        bg = Image.new("RGBA", im.size, bg_colour + (255,))
        bg.paste(im, mask=alpha)
        return bg
    else:
        return im


def watermark_img(img_file, template, dpi, quality):
    """Watermarks image with watermark template

    Args:
        img_file (str): path to image file
        template (PIL.Image.Image): watermark template
        dpi (int): dots per inch

    Returns:
        str: path to watermarked image
    """

    # Open image
    image = Image.open(img_file).convert('RGBA')
    image = remove_transparency(image)
    width, height = image.size

    # Apply watermark
    cropbox = ((template.size[0] - width) // 2,
               (template.size[1] - height) // 2,
               (template.size[0] + width) // 2,
               (template.size[1] + height) // 2)
    out = Image.alpha_composite(image, template.crop(cropbox)).convert('RGB')

    # Save image as PDF and delete original image file
    pdf_file = os.path.splitext(img_file)[0] + '.pdf'
    out.save(pdf_file, resolution=dpi, quality=quality)
    if os.path.isfile(img_file):
        os.remove(img_file)

    return pdf_file


def combine_all_pdfs(pdf_pages, out_dir, combined_postfix='_w'):
    """Merges single page PDFs to one combined PDF

    Args:
        pdf_pages (list): list of paths to single page PDF
        out_dir (str): path to output directory
        combined_postfix(str): postfix of combined (merged) PDF

    Returns:
        str: path to combined PDF
    """
    # Merge single pages to one PDF
    # mergedObject = PdfFileMerger()
    mergedObject = Pdf.new()   # create a blank PDF
    for pdf_page in pdf_pages:
        src = Pdf.open(pdf_page)
        mergedObject.pages.extend(src.pages)

    # Create file name of merged PDF
    pdf_name = os.path.basename(pdf_pages[0])  # remove full path
    pdf_name = os.path.splitext(pdf_name)[0]   # remove extension '.pdf'
    pdf_name = pdf_name.rsplit('_', 1)[0]       # remove '_000'
    pdf_file = os.path.join(out_dir, pdf_name + combined_postfix + '.pdf')

    # Save merged PDF
    mergedObject.save(pdf_file)

    return pdf_file


def watermark_pdf(input_dir, tmp_dir, output_dir,
                  fontsize, dpi, quality, imgmgckdir, pdf_file):
    """Watermarks each page of a given PDF file

    Args:
        input_dir (str): path to input directory
        tmp_dir (str): path to temporary directory
        output_dir (str): path to output directory
        dpi (int): dots per inch
        pdf_file (str): path to PDF file

    Returns:
        str: path to watermarked PDF containing watermark on every page
    """
    # Check folders
    if imgmgckdir is not None:
        if not os.path.exists(imgmgckdir):
            os.makedirs(imgmgckdir)
    try:
        with TemporaryDirectory(prefix='imgmgcktmp_', dir=imgmgckdir) as tempdir:
            with set_env(MAGICK_TEMPORARY_PATH=tempdir):
                # Converting each page of available PDFs into images
                # img_files = convert_pdf_to_img(pdf_file, input_dir, tmp_dir, dpi)
                img_files = convert_pdf_to_img(pdf_file, input_dir, tmp_dir, dpi)
                
                # Extracting matriculation numbers
                matnum = matnum_utils.get_matnum(pdf_file)

                # Watermarking PDF page images
                # Create template for first page
                
                template = create_watermark_template(img_files[0], matnum, fontsize, dpi)
                pdf_files = []
                for img_file in img_files:
                    pdf_file = watermark_img(img_file, template, dpi, quality)
                    pdf_files.append(pdf_file)
    except NotADirectoryError:
        pass

    # Combining watermarked PDF pages into one PDF
    if len(pdf_files) > 0:
        pdf_files.sort()
        watermarked_pdf = combine_all_pdfs(pdf_files, output_dir)
    else:
        raise Exception("{}: No PDF pages found".format(pdf_file))

    for pdf_file in pdf_files:
        os.remove(pdf_file)
    return watermarked_pdf


def _make_parser():
    parser = argparse.ArgumentParser(
        description=__doc__, prog='watermark.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "infolder", help="Input folder with PDFs to be watermarked")
    parser.add_argument(
        "outfolder", help="Output folder with watermarked PDFs")

    parser.add_argument(
        "-f", "--fontsize", default="75",
        help="Font size of watermark text in points")
    parser.add_argument(
        "-c", "--cores", default="1",
        help="Number of cores for parallel processing")
    parser.add_argument(
        "-t", "--tmp", default="./tmp", help="Temporary folder")
    parser.add_argument(
        "-d", "--dpi", default="150",
        help="Dots-per-inch parameter for PDF to image conversion")
    parser.add_argument(
        "-q", "--quality", default="75",
        help="quality parameter for JPEG compression of watermarked PDF page")
    parser.add_argument(
        "-z", "--imgmgc", default=None,
        help="Set temporary directory for Imagemagick")

    return parser


# Create argument parser with default values
_parser = _make_parser()


def main(args):
    """Main routine

    For all PDFs in ./pdfs folder:
    1) Convert each page of the PDFs into image
    2) Watermark each image
    3) Convert each image into single page PDFs
    4) Merge PDFs to one combined PDF
    """

    # Argument handling
    args = _parser.parse_args(args)
    infolder = args.infolder
    outfolder = args.outfolder
    tmpdir = args.tmp
    cores = int(args.cores)
    fontsize = int(args.fontsize)
    dpi = int(args.dpi)
    quality = int(args.quality)
    imgmgckdir = args.imgmgc

    # Set temporary directory for Imagemagick as Environment Variable.
    print('This directory might run out of space if there is not enough or submissions have many (150+) pages. Please keep a watch.')
    # Print status
    starttime = time.time()
    pdf_folder = os.listdir(infolder)
    pdf_files = [
        _ for _ in pdf_folder
        if _.lower().endswith(".pdf") and matnum_utils.starts_with_matnum(_)]

    if len(pdf_files) > 0:
        print("""
Available PDFs to be watermarked:
- {}

Files in output folder {} will be overwritten during this process.
Parallel execution with {:d} cores from now on.
    """.format("\n- ".join(pdf_files), outfolder, cores))
    else:
        print("""
There are no PDFs in the given directory.
Exiting now.""")
        return

    # Check folders
    if not os.path.exists(outfolder):
        os.makedirs(outfolder)

    if not os.path.exists(tmpdir):
        raise FileNotFoundError(
            "Temporary folder {tmp} does not exist.".format(tmp=tmpdir))

    # Call watermarking pipeline in parallel
    if cores > 1:
        pool = Pool(cores)
        watermark_fun = partial(
            watermark_pdf, infolder, tmpdir, outfolder, fontsize, dpi, quality, imgmgckdir)
        pdf_files_w = pool.map(watermark_fun, pdf_files)
        pool.close()
        pool.join()
    else:
        pdf_files_w = []
        for pdf_file in pdf_files:
            pdf_file_w = watermark_pdf(
                infolder, tmpdir, outfolder, fontsize, dpi, quality, imgmgckdir, pdf_file)
            pdf_files_w.append(pdf_file_w)

    if imgmgckdir is not None:
        try:
            reg_compile = re.compile("imgmgcktmp_")
            for root, dirs, files in os.walk(imgmgckdir, topdown=False):
                for name in dirs:
                    if  reg_compile.match(name):
                        os.rmdir(os.path.join(root, name))
        except PermissionError:
            pass
        
    # Print status
    endtime = time.time()
    print("""All PDFs are watermarked and can be found in {} folder:
- {}

Time taken: {:.2f}s
    """.format(outfolder, "\n- ".join(pdf_files_w), endtime-starttime))


if __name__ == '__main__':
    main(sys.argv[1:])